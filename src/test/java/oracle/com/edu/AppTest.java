package oracle.com.edu;


import oracle.com.edu.model.*;
import oracle.com.edu.util.HibernateUtil;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest {
    private static final Logger logger = LoggerFactory.getLogger(AppTest.class);
    private final Session session = HibernateUtil.getSessionFactory().openSession();

    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void testGetOrderById() {
        session.beginTransaction();
        OrdersEntity testOrder = session.get(OrdersEntity.class, 85L);
        logger.info(testOrder.getCustomersEntity().toString());
        logger.info(testOrder.getOrderDate().toString());
    }

    @Test
    public void testInventories() {
        session.beginTransaction();
        InventoriesEmbeddable inventoryUniq = new InventoriesEmbeddable(263L, 8L);
        InventoriesEntity testInventory = session.get(InventoriesEntity.class, inventoryUniq);
        logger.info(testInventory.getQuantity().toString());
    }

    @Test
    public void getProductsByCategory() {
        session.beginTransaction();
        ProductCategoriesEntity categoriesEntity = session.get(ProductCategoriesEntity.class, 4L);
        categoriesEntity
                .getProductsEntityList()
                .stream()
                .map(ProductsEntity::getProductName)
                .forEach(System.out::println);
    }

    @Test
    public void testOrderItems() {
        session.beginTransaction();
        OrdersEntity testOrder = session.get(OrdersEntity.class, 46L);
        testOrder
                .getOrderItemsList()
                .stream()
                .map(oi -> {
                    return oi.getOrderItemsEmbeddable().getItemOrderId();
                })
                .forEach(System.out::println);

    }

    @Test
    public void testOrderCustomers() {
        session.beginTransaction();
        OrdersEntity testOrder = session.get(OrdersEntity.class, 46L);
        logger.info(testOrder.getCustomersEntity().getAddress());
    }

    @Test
    public void testContacts() {
        session.beginTransaction();
        ContactsEntity testContacts = session.get(ContactsEntity.class, 90L);
        logger.info(testContacts.getCustomer().getAddress());
    }

    @Test
    public void testPayments() {
        session.beginTransaction();
        PaymentsEntity testPayment = session.get(PaymentsEntity.class, 1L);
        logger.info(testPayment.getAmmount().toString());
    }

    @Test
    public void testCountries(){
        session.beginTransaction();
        CountriesEntity testCountry = session.get(CountriesEntity.class, "US");
        logger.info(testCountry.getCountryName());
    }
}

























