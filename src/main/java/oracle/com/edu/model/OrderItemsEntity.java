package oracle.com.edu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "order_items")
public class OrderItemsEntity {

    @EmbeddedId
    private OrderItemsEmbeddable orderItemsEmbeddable;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("orderItemsId")
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    private OrdersEntity orderEntity;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "unit_price")
    private Double unitPrice;
}
