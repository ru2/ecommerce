package oracle.com.edu.model;

        import lombok.AllArgsConstructor;
        import lombok.Getter;
        import lombok.NoArgsConstructor;
        import lombok.Setter;

        import javax.persistence.*;
        import java.util.ArrayList;
        import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_categories")
public class ProductCategoriesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "category_name", length = 25, nullable = false)
    private String categoryName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productCategoriesEntity", cascade = CascadeType.ALL)
    private List<ProductsEntity> productsEntityList;

    @Override
    public String toString() {
        return "Locations:\n" +
                "id: " + categoryId +
                "name " + categoryName;
    }
}

