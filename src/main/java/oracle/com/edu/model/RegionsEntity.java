package oracle.com.edu.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "regions")
public class RegionsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "region_id")
    private Integer regionId;

    @Column(name = "region_name", length = 25, nullable = false)
    private String regionName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "regionId", cascade = CascadeType.ALL)
    private List<CountriesEntity> regionsEntities;

    @Override
    public String toString() {
        return "Locations:\n" +
                "id: " + regionId +
                "name " + regionName;
    }
}
