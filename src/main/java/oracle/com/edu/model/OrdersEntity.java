package oracle.com.edu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name="orders")
public class OrdersEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="order_id", updatable = false, nullable = false)
    private Long orderId;

    @OneToMany(mappedBy = "orderEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<OrderItemsEntity> orderItemsList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", updatable = false, nullable = false)
    private CustomersEntity customersEntity;

    @Column(name="status")
    private String status;

    @Column(name="salesman_id")
    private Long salesmanId;

    @Column(name="order_date")
    @Temporal(TemporalType.DATE)
    private java.util.Date orderDate;

    @OneToMany(mappedBy = "paymentsOrders", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PaymentsEntity> paymentOrdersList;
}
