package oracle.com.edu.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "employees")
public class EmployeesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id", length = 6, nullable = false, unique = true)
    private Integer employeeId;

    @Column(name="firstName", length = 20)
    private String firstName;

    @Column(name="lastName", length = 25, nullable = false)
    private String lastName;

    @Column(name="email", length = 25, unique = true)
    private String email;

    @Column(name="phone", length = 20)
    private String phone;

    @Column(name="hire_date")
    private Date hireDate;

    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="manager_id")
    private EmployeesEntity managerId;

    @OneToMany(mappedBy="managerId")
    private List<EmployeesEntity> subordinates;

    @Column(name="job_title")
    private String jobTitle;

    @Override
    public String toString() {
        return "Employee:\n" +
                "id: " + employeeId +
                "\nFirst Name: " + firstName + "\n" +
                "Last Name: " + lastName + "\n" +
                "Email: " + email + "\n" +
                "Hire Date: " + hireDate + "\n" +
                "Manager: " + managerId + "\n" +
                "Job Title: " + jobTitle + "\n";
    }

}
