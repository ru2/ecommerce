package oracle.com.edu.model;

        import lombok.AllArgsConstructor;
        import lombok.Getter;
        import lombok.NoArgsConstructor;
        import lombok.Setter;
        import javax.persistence.*;
        import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Embeddable
public class InventoriesEmbeddable implements Serializable {

    @Column(name="product_id")
    private Long productIdInventory;

    @Column(name="warehouse_id")
    private Long warehouseIdInventory;

}
