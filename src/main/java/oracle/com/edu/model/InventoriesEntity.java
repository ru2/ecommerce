package oracle.com.edu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name="inventories")
public class InventoriesEntity {

    @EmbeddedId
    private InventoriesEmbeddable inventoriesEmbeddable;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("productIdInventory")
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    private ProductsEntity product;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("warehouseIdInventory")
    @JoinColumn(name = "warehouse_id", referencedColumnName = "warehouse_id")
    private WarehousesEntity warehouse;

    @Column(name="quantity")
    private Long quantity;

}
