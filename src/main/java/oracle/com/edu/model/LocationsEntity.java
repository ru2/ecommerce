package oracle.com.edu.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "locations")

public class LocationsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id", length = 4, nullable = false, unique = true)
    private Integer id;

    @Column(name = "address", length = 40)
    private String streetAddress;

    @Column(name = "postal_code", length = 12)
    private String postalCode;

    @Column(name = "city", length = 30)
    private String city;

    @Column(name = "state", length = 25)
    private String state;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountriesEntity countryId;

    @OneToMany(mappedBy = "locationId")
    private List<WarehousesEntity> warehouseLocations;


    public String toString() {
        return "Location is: " + city
                + "Province is: " + state;
    }
}
