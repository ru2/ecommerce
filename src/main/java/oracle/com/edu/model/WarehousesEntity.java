package oracle.com.edu.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

    @Getter
    @Setter
    @NoArgsConstructor
    @Entity(name="WarehousesEntity")
    @Table(name = "warehouses")
    public class WarehousesEntity {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "warehouse_id")
        private String warehouseId;

        @Column(name = "warehouse_name", length = 40)
        private String warehouseName;

        @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JoinColumn(name = "location_id")
        private LocationsEntity locationId;

        @OneToMany(
                mappedBy = "warehouse",
                cascade = CascadeType.ALL,
                orphanRemoval = true
        )
        private List<InventoriesEntity> inventoriesEntityList = new ArrayList<>();

        @Override
        public String toString() {
            return "Country name: " + warehouseName;
        }
    }



