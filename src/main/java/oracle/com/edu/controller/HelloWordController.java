package oracle.com.edu.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class HelloWordController {
    @GetMapping("/index")
    public void index() {
        System.out.println("Greetings from Hello Controller");
    }
}
