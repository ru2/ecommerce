package oracle.com.edu.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.SessionFactoryRegistry;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

import java.util.Objects;

public class HibernateUtil {
    private static StandardServiceRegistry registry;
    private static SessionFactory sessionFactory = getSessionFactory();

    public static SessionFactory getSessionFactory(){
        if (Objects.isNull(sessionFactory)) {
            try {
                registry = new StandardServiceRegistryBuilder().configure().build();
                Configuration myConfiguration = new Configuration();
                myConfiguration.configure();

                sessionFactory = myConfiguration.buildSessionFactory(registry);

            } catch (Exception e) {
                e.printStackTrace();
                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);
                }
            }
        }
        return  sessionFactory;
    }
}
