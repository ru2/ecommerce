package oracle.com.edu.domain;

import lombok.*;

@Data
@Builder
@RequiredArgsConstructor

public class Customers {

    private final Long customerId;
    private final String name;
    private final String address;
    private final String website;
    private final Double creditLimit;

}
