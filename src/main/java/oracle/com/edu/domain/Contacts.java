package oracle.com.edu.domain;

import lombok.*;

@Data
@Builder
@RequiredArgsConstructor

public class Contacts {

    private final Long contactId;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Double phone;
    private final Long customerId;

}
