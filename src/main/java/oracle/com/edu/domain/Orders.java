package oracle.com.edu.domain;

import lombok.*;

import java.util.Date;

@Data
@RequiredArgsConstructor
@Builder

public class Orders {
    private final Long orderId;
    private final Long customerId;
    private final String status;
    private final Long salesmanId;
    private final Date orderDate;
}


