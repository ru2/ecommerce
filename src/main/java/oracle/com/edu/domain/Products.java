package oracle.com.edu.domain;
import lombok.*;

@Data
@Builder
@RequiredArgsConstructor

public class Products {
    private final Long productId;
    private final String productName;
    private final String description;
    private final Double standardCost;
    private final Double listPrice;
    private final Long categoryId;
}
