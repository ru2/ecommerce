package oracle.com.edu.domain;

import lombok.*;

@Builder
@RequiredArgsConstructor
@Data

public class OrderItems {
    private final Long orderId;
    private final Long itemId;
    private final Long productId;
    private final Long quantity;
    private final Double unitPrice;
}
