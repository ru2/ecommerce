package oracle.com.edu.domain;


import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@Data
@Builder
@RequiredArgsConstructor

public class Employees {

    private final Long employeeId;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Double phone;
    private final Date hireDate;
    private final Long managerId;
    private final String jobTitle;

}