package oracle.com.edu.domain;

import lombok.*;

@Data
@RequiredArgsConstructor
@Builder

public class Payments {
    private final Long paymentId;
    private final Long orderId;
    private final String paymentType;
    private final String paymentNote;
    private final String status;
    private final Double ammount;
}
