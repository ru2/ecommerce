# Ecommerce Stock and Store applications

This application source code can only be used for educational purposes. 

## Table of Contents

1. [ Description ](#desc)
2. [ General Install ](#setup)
3. [ TODO List ](#todo)

## 1. Description
This project was inspired as a concept of an E-commerce store for big companies. The main purpose is to create an application in order to check stock and delivery. This application supports the connections in different countries, departments and stores. This application enables accounting, taking in consideration the product stock quantity, invoice management and load delivery. The project is a concept for a big enterprise application consisting of 2 parts - server side and Web application (mobile app). TODO list contains tasks to be implemented in future.

Stack used in this project was Spring Boot Framework, Hibernate, Maven, Rest, Junit, Mockito, JPA, Oracle SQL database, Swagger, log4j2 librafy, Apache Tomcat, HTML, CSS, JSON, Docker. 

## 2. General Setup
Project requires Apache Maven to be installed on machine.

* Please use this article to Create and Configure a Container Database (CDB) in Oracle Database 12c
  https://oracle-base.com/articles/12c/multitenant-create-and-configure-container-database-12cr1
* Create user - ot and password - ot
* Please follow steps from this post to import database schema and data to pluggable from folder /db  
* Configure your application connection on hibernage.cfg.xml depending of your system (Unix or Windows)
* Run your application on api - http://localhost:8080/index (the log result is - Greetings from Hello Controller)

## 3. TODO list
There is a few main directions for new features that will be implemented:

* Creating REST for retrieving necessary data
* Implement Thymeleaf to better UI 
* Create ann mobile application that will interact to application